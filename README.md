# E-Commerce Project


## Installation

Use the package manager [pip](https://pip.pypa.io/en/stable/) to install this.

```bash
pip install -r requirements.txt
```


## Usage

```python
Postman Collection:
https://www.getpostman.com/collections/d98c1b660727a151a6be

API Docs:
https://documenter.getpostman.com/view/14116849/UVksKtHW
```


1. Admin => Admin has complete access to a product, he can update, delete, read-create.

2. Manager => They can create, update and read Products.

3. Employee => They can only read and create Products.

### Step 2: After login and OTP sent to email(currently use 4563 for testing) and then have API for verifying OTP if OTP is correct then the response contains user token and user details.

## Product

Model: Products model contains all field which is required and for adding color and size to a product, user needs to add first size and then he can add colors. The color model contains size which means product size is available on that color or not.

For product bulk upload users can upload size, colors, products from the admin panel and also have an API for bulk upload. I'll add a sample CSV file to the project for testing.
