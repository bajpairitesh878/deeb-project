from django.urls import path, include
from rest_framework.routers import DefaultRouter

# local import
from product_app.views import *
from product_app.product_import import BulkUploadView

router = DefaultRouter()
router.register('employer/view', ProductEmployerViewSet, basename='employee-product')
router.register('manager/view', ProductManagerViewSet, basename='manager-product')
router.register('admin/view', ProductAdminViewSet, basename='admin-product')
router.register('view', UserProductViewSet, basename='user-product')
router.register('size', ProductSizeViewSet, basename='size-product')
router.register('color', ProductColorViewSet, basename='color-product')

urlpatterns = [
    path('router/', include(router.urls)),
    path('publish/product', ProductPublishView.as_view(), name='publish-product'),
    path('upload', BulkUploadView.as_view(), name='upload-product')
]
