from rest_framework import serializers

# local import
from product_app.models import *


class SizeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductSize
        fields = '__all__'


class ColorSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProductColor
        fields = '__all__'
        depth = 1


class ProductManagerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        exclude = ('status',)


class ProductAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'

    def to_representation(self, instance):
        response = super().to_representation(instance)
        response['color'] = ColorSerializer(instance.color, many=True).data
        return response


class FileUploadSerializer(serializers.Serializer):
    file = serializers.FileField()
