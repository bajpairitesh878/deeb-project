from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

# local import
from product_app.models import *


# Register your models here.
@admin.register(Product)
class ProductAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'product_name', 'product_price', 'discount_price')
    list_display_links = ('id', 'product_name')
    search_fields = ('id', 'product_name')


@admin.register(ProductSize)
class ProductSizeAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'size')


@admin.register(ProductColor)
class ProductSizeAdmin(ImportExportModelAdmin, admin.ModelAdmin):
    list_display = ('id', 'color')
