from django.db import models

# local import
from accounts.models import ModelMixin
from uttils.custom_manager import ProductManager


# Create your models here.
class ProductSize(models.Model):
    objects = None
    size = models.CharField(max_length=10, default='')

    def __str__(self):
        return self.size


class ProductColor(models.Model):
    objects = None
    color = models.CharField(max_length=60, default='')
    size = models.ManyToManyField(ProductSize, blank=True, related_name='product_size')

    def __str__(self):
        return self.color


class Product(ModelMixin):
    product_name = models.CharField(max_length=250, default='')
    product_price = models.FloatField(default=False)
    discount_price = models.FloatField(default=False)
    product_fabric_type = models.CharField(max_length=150, default='')
    color = models.ManyToManyField(ProductColor, blank=True, related_name='product_color')
    product_description = models.TextField(null=True, blank=True)

    objects = models.Manager()
    product_objects = ProductManager()  # custom manager for active product

    def __str__(self):
        return f'{self.product_name}'

    class Meta:
        ordering = ['-id']
