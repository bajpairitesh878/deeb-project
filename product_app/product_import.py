from rest_framework import generics, permissions, status
from rest_framework.response import Response
import pandas as pd

# local import
from product_app.serializers import *


class BulkUploadView(generics.CreateAPIView):
    serializer_class = FileUploadSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        file = serializer.validated_data['file']
        reader = pd.read_csv(file)
        for _, row in reader.iterrows():
            new_file = Product(
                product_name=row["product_name"],
                product_price=row['product_price'],
                discount_price=row["discount_price"],
                product_fabric_type=row["product_fabric_type"],
                product_description=row["product_description"]
            )
            new_file.save()
            try:
                colors = row['color'].split(',')
                new_file.color.add(*colors)
            except Exception as e:
                pass
        return Response({"status": "success"},
                        status.HTTP_201_CREATED)
