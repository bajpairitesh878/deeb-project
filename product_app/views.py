from rest_framework import viewsets, status, permissions, views
from rest_framework.response import Response

# local import
from uttils.custom_permission import *
from product_app.serializers import *


# Create your views here.
class ProductEmployerViewSet(viewsets.ModelViewSet):
    """
    To get employee product, he can only create and read product
    """
    http_method_names = ('get', 'post')
    permission_classes = (permissions.IsAuthenticated, IsEmployerPermission)
    serializer_class = ProductManagerSerializer
    queryset = Product.objects.all()


class ProductManagerViewSet(viewsets.ModelViewSet):
    """
    Manager can update, read and create product
    """
    http_method_names = ('get', 'post', 'put')
    permission_classes = (permissions.IsAuthenticated, IsManagerPermission)
    serializer_class = ProductManagerSerializer
    queryset = Product.objects.all()


class ProductAdminViewSet(viewsets.ModelViewSet):
    """
    Manager can update, read and create product
    """
    permission_classes = (permissions.IsAuthenticated, IsAdminPermission)
    serializer_class = ProductAdminSerializer
    queryset = Product.objects.all()


class UserProductViewSet(viewsets.ModelViewSet):
    http_method_names = ('get',)
    permission_classes = (permissions.AllowAny,)
    queryset = Product.product_objects.all()
    serializer_class = ProductAdminSerializer


class ProductPublishView(views.APIView):
    permission_classes = (permissions.IsAuthenticated, IsAdminPermission)
    context = {}

    def put(self, request):
        query = Product.objects.filter(id=request.data['product_id'])
        if query:
            query.update(status=request.data['status'])
            self.context['message'] = 'Product Status Updated Successfully.'
            return Response(self.context)
        self.context['message'] = 'Product Id is not correct.'
        return Response(self.context, status=status.HTTP_400_BAD_REQUEST)


class ProductSizeViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = ProductSize.objects.all()
    serializer_class = SizeSerializer


class ProductColorViewSet(viewsets.ModelViewSet):
    permission_classes = (permissions.IsAuthenticated,)
    queryset = ProductColor.objects.all()
    serializer_class = ColorSerializer


# test