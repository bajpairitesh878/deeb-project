from rest_framework.permissions import BasePermission

# local import
from accounts.models import Profile


# custom method
def return_role(user):
    profile = Profile.objects.filter(created_by=user)
    if profile:
        return profile.last().user_role
    return None


class IsEmployerPermission(BasePermission):
    """
    Allow access to only active users that are permitted to perform CRUD operations
    """

    def has_permission(self, request, view):
        """
        To Employer user check
        For Employee - they can add and read product
        :param request: object
        :param view: object
        :return: boolean
        """
        profile = Profile.objects.get(created_by=request.user)
        return profile.user_role == 'Employee'


class IsManagerPermission(BasePermission):
    """
    Allow access to only active users that are permitted to perform CRUD operations
    """

    def has_permission(self, request, view):
        """
        To Employer user check
        For Employee - they can add and read product
        :param request: object
        :param view: object
        :return: boolean
        """
        role = return_role(request.user)
        return role == 'Manager'


class IsAdminPermission(BasePermission):
    """
    Allow access to complete operation for product
    """

    def has_permission(self, request, view):
        """
        To Employer user check
        For Employee - they can add and read product
        :param request: object
        :param view: object
        :return: boolean
        """
        role = return_role(request.user)
        return role == 'Admin'
