from django.apps import AppConfig


class UttilsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'uttils'
