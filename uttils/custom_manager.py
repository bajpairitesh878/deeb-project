from django.db import models


class ProductManager(models.Manager):
    """return only published object which status is active"""

    def get_queryset(self):
        return super().get_queryset().filter(status='active')
